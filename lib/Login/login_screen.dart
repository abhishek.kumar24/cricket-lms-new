import 'package:cricket_l_m_s/flutter_flow/flutter_flow_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key ?key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  InAppWebViewController? _webViewController;
  String url = 'https://demo1.profcymacrm.com/customer-login';
  bool isLoading = false;


    @override
    Widget build(BuildContext context) {
      return Scaffold(
          body: SafeArea(
            child: Stack(
              children: [
                InAppWebView(
               initialUrlRequest:
                 URLRequest(url: Uri.parse(url)),
                initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                  mediaPlaybackRequiresUserGesture: false,
                ),
              ),
                    onLoadStart: (InAppWebViewController controller,  url) {
                      setState(() {
                       url = url;
                       isLoading=true;
                      });
                    },
                    onLoadError: (
                        InAppWebViewController controller,
                         url,
                        int i,
                        String s
                        ) async {
                      print('CUSTOM_HANDLER: $i, $s');
                      showSnackbar(context, 'Something went wrong!');
                      Navigator.pop(context);

                    },
                    onLoadHttpError: (InAppWebViewController controller,  url,
                        int i, String s) async {
                      print('CUSTOM_HANDLER: $i, $s');
                      showSnackbar(context, 'Something went wrong!');
                      Navigator.pop(context);
                    },
                    onLoadStop:
                        (InAppWebViewController controller,  url) async {
                      setState(() {
                        url = url;
                        isLoading=false;
                      });
                    },
              onWebViewCreated: (InAppWebViewController controller) {
                _webViewController = controller;
                setState(() {
                  isLoading=false;
                });
              },
              androidOnPermissionRequest: (InAppWebViewController controller, String origin, List<String> resources) async {
                return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
              }

              ),
                Visibility(child: IconButton(onPressed:()=> Navigator.pop(context), icon: Icon(Icons.arrow_back,color: Colors.white,))),
                Center(
                  child: Visibility(
                      visible: isLoading, child: SpinKitWave(color: Colors.blue, type: SpinKitWaveType.start),
                  ),
                ),
            ]
            ),
          )
      );
    }
  }
