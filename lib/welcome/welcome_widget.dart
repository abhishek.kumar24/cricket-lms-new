import 'package:cricket_l_m_s/Login/login_screen.dart';
import 'package:permission_handler/permission_handler.dart';

import '../flutter_flow/flutter_flow_theme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WelcomeWidget extends StatefulWidget {
  const WelcomeWidget({Key ?key}) : super(key: key);

  @override
  _WelcomeWidgetState createState() => _WelcomeWidgetState();
}

class _WelcomeWidgetState extends State<WelcomeWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  int count=0;



  Future permission() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Permission.camera.request();
    await Permission.microphone.request();

  }



  @override
  void initState() {
    permission();
    super.initState();
    setState(() {
      count=0;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
   setState(() {
     count=0;
   });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Padding(
            padding: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    InkWell(
                      onTap: (){
                        setState(() {
                          count++;
                        });
                        if(count>=3){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginScreen()));
                        }
                      },
                      child: Text(
                        'Monday Watchlist',
                        style: GoogleFonts.getFont(
                          'Open Sans',
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(0, 16, 0, 0),
                    child: GridView(
                      padding: EdgeInsets.zero,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20,
                        childAspectRatio: 0.8,
                      ),
                      scrollDirection: Axis.vertical,
                      children: [
                        Image.asset(
                          'assets/images/grid_img.png',
                          width: double.infinity,
                          height: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          width: double.infinity,
                          height: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          width: double.infinity,
                          height: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        Image.asset(
                          'assets/images/grid_img.png',
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
