import 'dart:async';
import 'package:cricket_l_m_s/onboarding/onboarding_widget.dart';
import 'package:flutter/material.dart';

class SplashScreenWidget extends StatefulWidget {
  const SplashScreenWidget({Key ?key}) : super(key: key);

  @override
  _SplashScreenWidgetState createState() => _SplashScreenWidgetState();
}

class _SplashScreenWidgetState extends State<SplashScreenWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    super.initState();
    init();
  }



  init() async {
    Timer(const Duration(seconds: 3), ()
    {
      Navigator.pushReplacement<void, void>(context,
          MaterialPageRoute(builder: (context) => OnboardingWidget()));
    }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Image.asset(
            'assets/images/LMS.png',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
